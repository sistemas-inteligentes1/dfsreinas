#pragma once
#include <stdlib.h>
#include <iostream>
#include <ctime>
using namespace std;

class Reina
{

private:
	bool** validador;
	int** tablero;
	int NroSoluciones;
	int N;
	int cantNodos;
	clock_t time;
	int cantSol;

public:
	Reina(int NumeroReinas);
	~Reina();
	//Logica
	void EncontrarSoluciones();
	void BloquearDiagonales(int fila, int columna);
	void BloquearVerticalHorizontal(int fila, int columna);
	void BloquearTodos(int fila, int columna);
	int getCantNodosVisitados();
	int getCantSoluciones();

	void BusquedaProfundidad(int fila, int columna, int NroReina);
	void QuitarMarcados(int fila, int columna);
	//Mostrar

	void mostrarValidos();
	void mostrarTablero();


};

