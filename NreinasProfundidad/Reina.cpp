#include "Reina.h"

Reina::Reina(int NumeroReinas)
{

    //Iniciar Numero de Reinas
    N = NumeroReinas;
    NroSoluciones = 0;
    cantNodos = 0;
    cantSol = 0;;
    time = clock();
    //Crear un Vector
    validador = new bool* [NumeroReinas];
    tablero = new int* [NumeroReinas];

    //Crear la Matriz en base al Vector
    for (int i = 0; i < NumeroReinas; i++) {
        validador[i] = new bool[NumeroReinas];
        tablero[i] = new int[NumeroReinas];
        for (int j = 0; j < NumeroReinas; j++) {
            validador[i][j] = false;
            tablero[i][j] = 0;
        }
    }
}

Reina::~Reina()
{
}

void Reina::EncontrarSoluciones()
{  
    for (int i = 0; i < N; i++)
    {
        BusquedaProfundidad(i, 0, 1);
    }
    cout << "=======SOLUCIONES=======" << endl;
    cout << "Cantidad de soluciones:" << getCantSoluciones() << endl;
}

void Reina::BloquearDiagonales(int fila, int columna)
{
    //Iniciar Auxiliares 
    int filaAux, ColumAux;
    //Diagonal Positiva
    filaAux = fila;
    ColumAux = columna;
    while (filaAux > 0 && ColumAux > 0)
    {
        filaAux--;
        ColumAux--;
    }
    //Nivelacion para marcar las dispociones no validas
    filaAux++;
    ColumAux++;
    while (filaAux < N && ColumAux < N)
    {
        validador[filaAux][ColumAux] = true;
        filaAux++;
        ColumAux++;
    }
    //Diagonal Negativa
    filaAux = fila;
    ColumAux = columna;
    while (filaAux < N && ColumAux > 0)
    {
        filaAux++;
        ColumAux--;
    }
    //Nivelacion
    filaAux--;
    ColumAux++;
    while (filaAux >= 0 && ColumAux < N)
    {
        validador[filaAux][ColumAux] = true;
        filaAux--;
        ColumAux++;
    }
}

void Reina::BloquearVerticalHorizontal(int fila, int columna)
{
    //Declarar Variables Auxiliares
    int filaAux, ColumAux;
    //Inicar Variables Locales
    ColumAux = columna;
    filaAux = 0;
    while (filaAux < N)
    {
        validador[filaAux][ColumAux] = true;
        filaAux++;
    }
    ColumAux = 0;
    filaAux = fila;
    while (ColumAux < N)
    {
        validador[filaAux][ColumAux] = true;
        ColumAux++;
    }

}

void Reina::BloquearTodos(int fila, int columna)
{
    BloquearVerticalHorizontal(fila, columna);
    BloquearDiagonales(fila, columna);
}

void Reina::BusquedaProfundidad(int fila, int columna, int NroReina)
{
    tablero[fila][columna] = 1;
    cantNodos++;
    BloquearTodos(fila, columna);
    if (NroReina == N)
    {
        cantSol++;
        mostrarTablero();
        time = clock() - time;
        cout << "Tiempo de ejecucion:" << (float)time / CLOCKS_PER_SEC << "segundos" << endl;
        cout << "Cantidad de nodos visitados:" << getCantNodosVisitados() << endl;
        //system("pause");
    }
    else
    {
        for (int i = 0; i < N; i++)
        {
            if (validador[i][columna + 1] == false)
            {
                BusquedaProfundidad(i, columna + 1, NroReina + 1);
            }
        }
    }
    QuitarMarcados(fila, columna);
}

void Reina::QuitarMarcados(int fila, int columna)
{
    tablero[fila][columna] = 0;
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            validador[i][j] = false;
        }
    }

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            if (tablero[i][j] == 1)
            {
                BloquearTodos(i, j);
            }
        }
    }
}


void Reina::mostrarValidos()
{
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            cout << validador[i][j];
        }
        cout << endl;
    }
}

void Reina::mostrarTablero()
{
    cout << "Es Solucion" << endl;
    /*for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            cout << tablero[i][j] << " ";
        }
        cout << endl;
    }*/
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            if (tablero[i][j] == 1)
            {
                cout << "[" << i + 1 << "," << j + 1 << "]";
            }
        }

    }
    cout << endl;
}
int Reina::getCantNodosVisitados() {
    return cantNodos;
}
int Reina::getCantSoluciones() {
    return cantSol;
}